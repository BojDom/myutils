const {applyPatch,applySnapshot,getSnapshot,onPatch,unprotect,onSnapshot,destroy,types } = require('mobx-state-tree');
const {Subject} = require('rxjs');
const {bufferTime,filter} = require('rxjs/operators');
const {throttle,get,set} = require('lodash');
const {observable} = require('mobx');

class SyncServer {

    constructor(scServer){
        this.data = {};
        this.subs = {};
        this.patch = {};
        this.patchSubs = {}
        scServer.addMiddleware(scServer.MIDDLEWARE_PUBLISH_IN,function(req, next) {
				
                let splitted = req.channel.split('/');
                if (splitted[0]&&splitted[0]=='priv'){
                    if (splitted[1]&&splitted[1]==req.socket.authToken._id) next();
                    else next('not.auth')
                }
                else {
                    set(req,'data.usrId', req.socket.authToken && req.socket.authToken._id)
                    next()
                }
            }
        );
        scServer.addMiddleware(scServer.MIDDLEWARE_SUBSCRIBE,function(req, next) {
            if (!req.socket.authToken) return;
                let splitted = req.channel.split('/');
                if (splitted[0]&&splitted[0]=='priv'){
                    if (splitted[1]&&splitted[1]==req.socket.authToken._id) next();
                    else next('not.auth')
                }
                else 
                next()
        });
        this.scServer = scServer;
    }
    sync({channel,mstStructure,initialData,options,store}){
        if (!options) options = {}
        if (!store) {
            this.data[channel] = mstStructure().create(initialData);
            this.scServer.exchange.publish(`${channel}/snapshot`,initialData);
            if (options.unprotected) unprotect(this.data[channel])
        }
        else this.data[channel] = store;

        if (options.onSnapshot) {
            onSnapshot(this.data[channel],throttle( data=>{
                options.onSnapshot(data)
            },1000 ))
        }
        else {
            this.patch[channel] = new Subject();
            this.patch[channel].pipe(bufferTime(100),filter(a=>a.length)).subscribe(a=>{
                this.scServer.exchange.publish(`${channel}/patch`,a);
            });
        }

        onPatch(this.data[channel],d=>{
            if (!(options && options.excludePatch && options.excludePatch.includes(d.name)))
            this.patch[channel].next(d)
        });
            
        this.subs[ channel + '_snap' ] = this.scServer.exchange.subscribe(channel + '/getSnapshot');
        this.subs[ channel + '_snap' ].watch(d=>{
            let data = {snap:getSnapshot( this.data[channel] )}
            if (d.mst)
                data.mst = mstStructure.toString();
            this.scServer.exchange.publish(`priv/${d.usrId}/${channel}/snap`,data)
        })
    }
    close(channelName){
        try{
            Object.keys(this.subs).forEach(k=>{
                this.subs[k].unsubscribe()
                this.subs[k].unwatch()
            })
            destroy(this.data[channelName])
        }catch(e){
            console.log(`error closing channel ${channelName}`)
        }
    }
}

class SyncClient  {
   
    data = observable.map({})
    constructor(socket){
        this.subs= {}
        this.socket = socket;
    }
    async sync({channel,options}){
        if (!options) options = {}
        
        this.subs[channel+'_snap']= this.socket.subscribe(`priv/${this.socket.authToken._id}/${channel}/snap`);
        this.subs[channel+'_snap'].on('subscribe',()=>{
            console.log('subscribed: ', channel)
            this.socket.publish(channel + '/getSnapshot',{mst:!this.data.get(channel)});
            this.subs[channel+'_patch']= this.socket.subscribe(`${channel}/patch`);
            this.subs[channel+'_patch'].watch(data=>{
                if (!this.data.get(channel)) return;
                if (options.onPatch) 
                    options.onPatch(data)
                else applyPatch(this.data.get(channel),data)
            })
        })
        this.subs[channel+'_snap'].watch(data=>{ 
            if (options.onSnapshot)
                options.onSnapshot(data.snap)
            else {
                if (data.mst)
                    this.data.set(channel, eval(data.mst)().create(data.snap));
                else
                    applySnapshot(this.data.get(channel),data.snap)
            }
        })
    }
    async close(channelName){
        try{
            Object.keys(this.subs).forEach(k=>{
                this.subs[k].unsubscribe()
                this.subs[k].unwatch()
            })
            destroy(this.data.get(channelName))
        }catch(e){
            console.log(`error closing channel ${channel} ${e}`)
        }
    }

}

module.exports = {
    SyncServer,
    SyncClient
}