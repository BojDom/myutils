const {join} = require("path"),
	{NginxConfFile} = require('nginx-conf'),
	baseFile = join(__dirname, 'nginx.base'),
	{promises:{appendFile,readFile}} = require('fs'),
	baseSslFile = join(__dirname, 'nginx.basessl');

module.exports.http = ({DOMAIN,HTTP_PORT,SSL_PORT,ROOT_CERT,DOMAINS}) => {
	return new Promise(r=>{
		NginxConfFile.create(baseFile, (err, conf)=>{
			let outFile = join( `${ROOT_CERT}`,`/certs`,'nginx.conf');
			conf.die(baseFile)
			conf.nginx._add('upstream', DOMAIN);
			conf.nginx._add('upstream', 'http_' + DOMAIN);
			conf.nginx.upstream[0]._add('server', '0.0.0.0:' + SSL_PORT)
			conf.nginx.upstream[1]._add('server', '0.0.0.0:' + HTTP_PORT)
			conf.nginx.server._add('server_name', (DOMAINS ? DOMAINS.split(',').join(' ') : DOMAIN) + ' www.'+DOMAIN)
			conf.nginx.server.location._add('proxy_pass', 'http://http_'+DOMAIN)
			conf.live(outFile)
			setTimeout(r,200)
		})
	})
}

module.exports.ssl =  async ({DOMAIN,ROOT_CERT,NGINX_FOLDER,DOMAINS,MAINTENANCE}) => {
	let outFile = join( `${ROOT_CERT}`, `certs`,'nginx.conf');	
	await appendFile(outFile, await readFile(baseSslFile))
	return await new Promise(r=>{
		NginxConfFile.create(outFile, function(err, conf) {
			conf.nginx.server[1].location[1]._add('proxy_pass', 'https://'+DOMAIN)
			if (MAINTENANCE) {
				/* conf.nginx.server[1].location._add('error_page', `502 /${MAINTENANCE.split('/').pop()}`)
				conf.nginx.server[1]._add('location',  `= /${MAINTENANCE.split('/').pop()}`)
				conf.nginx.server[1].location[1]._add('internal')
				conf.nginx.server[1].location[1]._add('root', MAINTENANCE.split('/').slice(0,-1).join('/')) */
			}
			conf.nginx.server[1]._add('server_name', (DOMAINS ? DOMAINS.split(',').join(' ') : DOMAIN) + ' www.'+DOMAIN)
			conf.die(baseFile)
			if (conf.nginx.server[1].ssl) return console.error('no ssl nginx directive');
			conf.nginx.server[1]._add('ssl_certificate',
			join(NGINX_FOLDER, DOMAIN, 'live',DOMAIN, 'fullchain.pem'));
			conf.nginx.server[1]._add('ssl_certificate_key',
			join(NGINX_FOLDER, DOMAIN, 'live',DOMAIN , 'privkey.pem'))
			conf.live(outFile)
			setTimeout(r,200)
		})	
	})
}
