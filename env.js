const {writeFile} = require('node:fs/promises'),
      {existsSync} = require('fs'),
      {exec} = require('child_process'),
      {promisify} = require('util'),
      execp = promisify(exec),
      {join} = require('path');

;(async ()=>{
  console.log('fetching envs from tbot', process.env.APP_NAME ,'ENV: ', process.env.ENV )
  let r = await fetch(`https://${process.env.TELEGRAM_URL}/envs`,{
    headers: {'Content-Type': 'application/json'},
    method:'POST',
    body: JSON.stringify({
      app: process.env.APP_NAME,
      env: process.env.ENV,
    })
  });
  let {envs,dbCerts,firebase_credentials, certs} = await r.json();

  if (dbCerts?.MONGO_CLIENT_PEM && envs.MONGO_CLIENT_PEM){
    let root = '/'+envs.MONGO_CLIENT_PEM.split('/')[1]+'/';
    if (!existsSync( root ))
      root = '/app/'

    console.log('writing PEM ON ', root + (envs.MONGO_CLIENT_PEM.split('/').slice(2).join('/')))

    await writeFile( root + (envs.MONGO_CLIENT_PEM.split('/').slice(2).join('/')) , dbCerts.MONGO_CLIENT_PEM );
    if (!envs.MONGO_CA)
      envs.MONGO_CA = envs.MONGO_CLIENT_PEM.split('/').slice(0,-1).join('/') + '/ca.crt';
    await writeFile(  root+ (envs.MONGO_CA.split('/').slice(2).join('/'))  , dbCerts.MONGO_CA)
  }


  if (firebase_credentials?.[envs.GOOGLE_APPLICATION_CREDENTIALS] ) {
    let out =  join( process.cwd() , envs.GOOGLE_APPLICATION_CREDENTIALS );
    await writeFile( out ,
      JSON.stringify(firebase_credentials[envs.GOOGLE_APPLICATION_CREDENTIALS] ,null,2));
    await execp('chmod 644 '+ out );
      console.log('FIREBASE CREDENTIAL WRITTEN',out);
  }

  envs = Object.keys(envs).map(k=>{
    return k+'='+envs[k];
  }).join("\r\n");

  console.log('envs',envs);
  let envOutFile = process.env.ENV_FILE || '.env'
  if (!envOutFile)
    join(process.cwd(),process.env.ENV_FILE || '.env')


  await writeFile( envOutFile,envs)
  console.log('writed env on ', process.cwd())
  process.exit(0)
})().catch(console.error);
