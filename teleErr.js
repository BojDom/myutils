const axios = require("axios");
const https = require("https"),
	cj = require("circular-json").stringify;

const api = axios.create({
	baseURL: 'https://' + process.env.TELEGRAM_URL,
	timeout: 3000,
	withCredentials: true,
	//transformRequest: [(data) => JSON.stringify(data.data)],
	//httpsAgent: new https.Agent({ keepAlive: true ,  rejectUnauthorized: false}),
	headers: {
		Accept: "application/json",
		"Content-Type": "application/json"
			//'User-Agent':'chrome'
	}
});
//spezza stringhe 
function chunkString(str, length) {
	return str.match(new RegExp('.{1,' + length + '}', 'g'));
}
if (!("toJSON" in Error.prototype))
	Object.defineProperty(Error.prototype, "toJSON", {
		value: function() {
			var alt = {};

			Object.getOwnPropertyNames(this).forEach(function(key) {
				alt[key] = this[key];
			}, this);

			return alt;
		},
		configurable: true,
		writable: true
	});

let typesOfLog = ['log', 'error'];

const teleErr = function(msg, type) {
	if (typeof type != 'string' || typesOfLog.indexOf(type) === -1) type = 'log';
	if (typeof msg == 'undefined') msg = ['undefined'];
	else msg = chunkString(cj(msg), 4095);
	msg.map(m => {
		api.post("toMe", {
				msg: m,
				type: type
			})
			.catch(err => {
				console.log("NON INVIATO A TELEGRAM", err, msg);
			});
	})


};
module.exports = teleErr;
