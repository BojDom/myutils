const {observable, computed , configure} = require('mobx'),
    {MongoClient} = require('mongodb'),
    {readFileSync,existsSync} = require('fs'),
    {cloneDeep, omit} = require('lodash'),
    redis = require('redis'),
    chalk = require('chalk');

configure({
    enforceActions:"never",
    isolateGlobalState: true
})

class dbConnect {
    constructor() {
        this.obs = observable({m:false,rs0:false,currentDb:'m'},{},{deep:false});
        this.m = computed(()=>this.obs[this.obs.currentDb])
        //this.mongoConnect({name:'m'})
        //this.redisConnect({name:'redis'})
    }

    async mongoConnect(config = {}){
        let conf;
        const handleError = async (e)=>{
            console.log(chalk.bgRed(conf?.url,e));
            setTimeout(()=>{
                this.mongoConnect(config)
            },5000)
        }

        try {
            if (this.obs[config.name] ) {
                console.log(chalk.bgMagenta('sovrascrivendo',config.name))
                this.obs[config.name].close?.();
                this.obs[config.name]=false
            }
            conf = cloneDeep({
                url:process.env.MONGO_URL || `mongodb://s3.edomi.it:7711,s2.edomi.it:7711,s1.edomi.it:7711`,
                readPreference:'secondaryPreferred',
                ...config
            })
            
            if (config.ssl) {
                if (process.env.MONGO_CLIENT_PEM && !existsSync(process.env.MONGO_CLIENT_PEM)) {
                    console.error('no mongo client.pem ',process.env.MONGO_CLIENT_PEM )
                    return;
                }
                Object.assign(conf,{  
                    key: readFileSync(conf.key || process.env.MONGO_CLIENT_PEM),
                    cert: readFileSync(conf.key || process.env.MONGO_CLIENT_PEM),
                    ca: readFileSync(conf.ca ||process.env.MONGO_CA_PEM || (process.env.MONGO_CLIENT_PEM.split('/').slice(0,-1).join('/'))+'/ca.crt'),
                    //authMechanism:"MONGODB-X509",
                    //auth: { username:process.env.MONGO_USER, password:process.env.MONGO_PWD },
                    tls:true,
                    ssl:true
                })
                if (!conf.password)
                    conf.authMechanism="MONGODB-X509"
            }
            
            const conn =  await MongoClient.connect(conf.url , omit(conf, ['url','name']))
            console.log(chalk.green('mongo connesso',config.name,conf.url.includes('@') ? conf.url.split('@')[1] : conf.url ))

            conn.on('error',(e)=>{
                handleError(e,config);
            })
            conn.on('close',()=>{
                handleError('CLOSED');
            })
            this.obs[config.name || 'm']= conn
            if (this.obs.currentDb=='m') this.obs.currentDb=config.name
        }

        catch(e){
          handleError(e)
        };
    }

    async redisConnect(conf = {}){
        const r = redis.createClient({
            socket:{
                port: conf.port || process.env.REDIS_PORT, // Redis port
                host: conf.host || process.env.REDIS_HOST || '0.0.0.0', // Redis host
            },
            //username: process.env.REDIS_USR,
            password: conf.pwd ||  process.env.REDIS_PWD,
            ...conf
        })
        r.on('connect', _ => {
            console.log('redis connesso')
            this.obs[conf.name || 'redis'] = r
        })
        r.on('error', (e) => {
            console.error('redis error', e)
            this.obs[conf.name || 'redis'] = false  
        })
        r.on('disconnect', () => {
            console.error('redis disconnected')
            this.obs[conf.name || 'redis'] = false
        })
        r.connect();
    }
}
module.exports = new dbConnect()
