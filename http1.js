const {exec} = require('child_process'), 
  jwt = require('jsonwebtoken'),
  {pick} = require('lodash'),
  scClient = require('socketcluster-client'),
  boot = require('./nginx/boot'),
  express = require('express'),
  {existsSync} = require('fs'),
  ge = require('@root/greenlock-express'),
  Greenlock = require('@root/greenlock');

let sc , data, greenlock;
const app = express();
let root = '/app';
const {APP_NAME, DOMAINS ,DOMAIN } = process.env;	
(async () => {
  if (existsSync(`/${APP_NAME}`)) {
    root = `/${APP_NAME}`
  }
  await new Promise(r => {
    exec(`node ${__dirname}/env`, (err, out, outerr) => {
      console.log('env out', root, out ,err, outerr);
      require('dotenv').config({path:`${root}/${process.env.ENV_FILE|| '.env'}`});
      r()
    })
  })

  console.log('domain', DOMAINS, DOMAIN , 'appname',APP_NAME)
  greenlock = Greenlock.create({
    packageAgent: `${APP_NAME}/1.0.0`,
    configFile: `${root}/certs/config.json`,
    maintainerEmail: 'admin@edomi.it',
    subscriberEmail: 'admin@edomi.it',
    debug:false,
    notify: function (event, details) {
      console.log('NOTIFY', event, details)
      if ('cert_issue' === event) {
        certDone()
      }
    }
  });


  var scConfig = {
    host: '0.0.0.0:' + process.env.WSPORT,
    query: {t: jwt.sign({app: APP_NAME}, process.env.JWT_NGINX_KEY, {algorithm: 'HS512'})}
  }

  sc = scClient.connect(scConfig)
  sc.on('error', (e) => {
    console.error(e, scConfig, DOMAIN)
  })
  sc.on('connect', async () => {

    data = pick(process.env,['HTTP_PORT','SSL_PORT','SOCKETCLUSTER_PORT','DOMAIN','DOMAINS','NGINX_FOLDER', 'MAINTENANCE']);
    if (!data.SSL_PORT) data.SSL_PORT= data.SOCKETCLUSTER_PORT; 
    data.ROOT_CERT = root;

    console.log('CONNECTED TO NGINX', data);
    await boot.http(data);
    sc.emit('nginxReload');
    requestCerts()
  });

})();


async function requestCerts() {
  ge.init(() => ({greenlock})).serve((glx, bo) => {
    let httpServer = glx.httpServer(app);
    httpServer.listen(process.env.HTTP_PORT, () => {
      console.log('HTTP exp',data)
    })

    app.get('/test',(req,res,next)=>{
      res.end('ok')
    })
    app.get('*',(req,res,next)=>{
      console.log('requrl http',req.url, req.headers['x-real-ip']);
      res.redirect('https://' + req.headers.host + req.url);
    })
  });

  let altnames = DOMAINS ? DOMAINS.split(',') :  [ DOMAIN];

  if (DOMAIN.split('.').length==2)
    altnames.push('www.'+DOMAIN);
    
  await greenlock.sites.add({
    subject: DOMAIN,
    altnames,
    env: 'live',
    store: {
      module: "greenlock-store-fs",
      basePath: `${root}/certs`
    }
  })
  console.log('ADDD live', arguments);

	let site = await greenlock.get( { servername:DOMAIN} ) 
    if (!site) {
        console.log(servername + ' was not found in any site config');
        return;
    }
    certDone()
};

async function certDone() {
  await boot.ssl(data);
  sc.emit('nginxReload');
  await new Promise(res => setTimeout(res, 800));
  exec(`pm2 start pm2.config.js --only app --env ${(process.env.ENV||process.env.NODE_ENV).startsWith('dev') ? 'dev' : 'prod'}`, (err, stdout, sterr) => {})
  sc.disconnect();
  sc.destroy();
}