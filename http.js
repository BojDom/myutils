const {exec} = require('child_process'), 
  jwt = require('jsonwebtoken'),
  {pick} = require('lodash'),
  {existsSync} = require('fs'),
  ge = require('@root/greenlock-express'),
  Greenlock = require('@root/greenlock');

let root = '/app', nConfig, envs;
let {APP_NAME} = process.env;

;(async () => {
  if (existsSync(`/${APP_NAME}`)) {
    root = `/${APP_NAME}`
  }
  await new Promise(r => {
    exec(`node ${__dirname}/env`, (err, out, outerr) => {
      console.log('env out', root, out ,err, outerr);
      envs = require('dotenv').config({path:`${root}/${process.env.ENV_FILE|| '.env'}`});
      r()
    })
  })
  const { DOMAINS ,DOMAIN, WSPORT , HTTP_PORT, JWT_NGINX_KEY , ENV  } = { ...process.env, ...envs}
  console.log('SERVING ', DOMAINS, DOMAIN , 'appname',APP_NAME, 'HTTP_PORT', HTTP_PORT, 'ENV: ', ENV, root)
  nConfig = {
    url: 'http://0.0.0.0:' + WSPORT,
    headers: { 'content-type': 'application/json', accept:'application/json', authorization:`Bearer ${await jwt.sign( {APP_NAME }, JWT_NGINX_KEY , {algorithm:'HS512'})}`}
  }

  data = pick({...process.env, ...envs},['HTTP_PORT','SSL_PORT','SOCKETCLUSTER_PORT','DOMAIN','DOMAINS','NGINX_FOLDER', 'MAINTENANCE', 'APP_NAME' , 'ENV']);
  if (!data.SSL_PORT) data.SSL_PORT= data.SOCKETCLUSTER_PORT; 

  greenlock = Greenlock.create({
    packageAgent: `${APP_NAME}${ENV}/1.0.0`,
    configFile: `${root}/certs/config.json`,
    maintainerEmail: 'admin@edomi.it',
    subscriberEmail: 'admin@edomi.it',
    debug:false,
    force:true,
    notify: function (event, details) {
      console.log('NOTIFY', event, details)
      if ('cert_issue' === event) {
        certDone()
      }
    }
  });

  
  let ok = await confNginx(data);

  console.log('okkk HTTP',ok);
  await new Promise(r=>setTimeout(r,1000));
  ge.init(() => ({greenlock})).serve((glx, bo) => {
    glx.httpServer((req,res)=>{
      console.log('req url', req.url)
      if (req.url.startsWith('/test')) {
        res.end('ok')
      }
    }).listen(HTTP_PORT, async () => {

        let altnames = DOMAINS ? DOMAINS.split(',') :  [ DOMAIN];

        if (DOMAIN.split('.').length<2)
          altnames.push('www.'+DOMAIN);
        console.log('ADDING' , DOMAIN);
        await greenlock.sites.add({
          subject: DOMAIN,
          altnames,
          env: process.env.STAGING || 'live',
          store: {
            module: "greenlock-store-fs",
            basePath: `${root}/certs`
          }
        })
        let certs = await greenlock.get( { servername:DOMAIN} )
        await certDone(certs)

    })
  });

})().catch((e)=>{
  console.error('CATCHED ERROR: ' + e.message);
  certDone();
  process.exit(1);
});

async function certDone(certs) {
  let ok = await confNginx({ ...data, ssl:1})
  console.log('OK',ok)
  if (certs)
  await fetch(`https://${process.env.TELEGRAM_URL}/setLEcerts`,{
    headers: nConfig.headers,
    method:'POST',
    body: JSON.stringify({certs})
  });
  process.exit()
}
async function confNginx(data){
 return await fetch(nConfig.url+'/http',{
    headers: nConfig.headers,
    method: 'POST',
    body: JSON.stringify(data) 
  }).then(r=>r.text());
}