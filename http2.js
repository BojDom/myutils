const {exec} = require('child_process'), 
  jwt = require('jsonwebtoken'),
  {pick} = require('lodash'),
  {existsSync} = require('fs'),
  ge = require('@root/greenlock-express'),
  Greenlock = require('@root/greenlock');

let sc ,  greenlock , nConfig;
let root = '/app';
const {APP_NAME, DOMAINS ,DOMAIN } = process.env;

;(async () => {
  if (existsSync(`/${APP_NAME}`)) {
    root = `/${APP_NAME}`
  }
  await new Promise(r => {
    exec(`node ${__dirname}/env`, (err, out, outerr) => {
      console.log('env out', root, out ,err, outerr);
      require('dotenv').config({path:`${root}/${process.env.ENV_FILE|| '.env'}`});
      r()
    })
  })
  nConfig = {
    url: 'http://0.0.0.0:' + process.env.WSPORT,
    headers: { 'content-type': 'application/json', accept:'application/json'}
}

  
  console.log('domain', DOMAINS, DOMAIN , 'appname',APP_NAME);
  data = pick(process.env,['HTTP_PORT','SSL_PORT','SOCKETCLUSTER_PORT','DOMAIN','DOMAINS','NGINX_FOLDER', 'MAINTENANCE']);
  if (!data.SSL_PORT) data.SSL_PORT= data.SOCKETCLUSTER_PORT; 
  nConfig.headers.authorization = `Bearer ${await jwt.sign( {APP_NAME }, process.env.JWT_NGINX_KEY , {algorithm:'HS512'})}`;

  greenlock = Greenlock.create({
    packageAgent: `${APP_NAME}/1.0.0`,
    configFile: `${root}/certs/config.json`,
    maintainerEmail: 'admin@edomi.it',
    subscriberEmail: 'admin@edomi.it',
    debug:false,
    notify: function (event, details) {
      console.log('NOTIFY', event, details)
      if ('cert_issue' === event) {
        certDone()
      }
    }
  });

  
  let ok = await confNginx(data)
  console.log('okkk HTTP',ok);
  await nginxReload()
  await new Promise(r=>setTimeout(r,1000));
  ge.init(() => ({greenlock})).serve((glx, bo) => {
    glx.httpServer().listen(process.env.HTTP_PORT, async () => {

        let altnames = DOMAINS ? DOMAINS.split(',') :  [ DOMAIN];

        if (DOMAIN.split('.').length==2)
          altnames.push('www.'+DOMAIN);
          
        await greenlock.sites.add({
          subject: DOMAIN,
          altnames,
          env: 'live',
          store: {
            module: "greenlock-store-fs",
            basePath: `${root}/certs`
          }
        })
        console.log('ADDD live');

        let site = await greenlock.get( { servername:DOMAIN} ) 
          if (!site) {
              console.log(servername + ' was not found in any site config');
              return;
          }
          certDone()
    })
  });

})().catch(console.error());

async function certDone() {
  let ok = await confNginx({ ...data, ssl:1})
  console.log('okkk ssl',ok);
  await nginxReload();
  exec(`pm2 stop all && pm2 start pm2.config.js --only app --env ${(process.env.ENV||process.env.NODE_ENV).startsWith('dev') ? 'dev' : 'prod'}`, (err, stdout, sterr) => {});

}

async function nginxReload() {
  return await fetch(nConfig.url+'/nginxReload', {
    headers: nConfig.headers,
  }).then(r=>r.text())
}
async function confNginx(data){
 return await fetch(nConfig.url+'/http',{
    headers: nConfig.headers,
    method: 'POST',
    body: JSON.stringify(data) 
  }).then(r=>r.text());
}